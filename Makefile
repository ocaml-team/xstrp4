# make all: 		make bytecode archive
# make install: 	install bytecode archive
# make uninstall: 	uninstall package
# make clean: 		remove intermediate files
# make distclean: 	remove any superflous files
# make release: 	cleanup, create archive, tag CVS module 
#			(for developers)


include Makefile.conf

#----------------------------------------------------------------------
# specific rules for this package:

OBJECTS  = xstrp4_here_types.cmo xstrp4_here_lexer.cmo xstrp4_here.cmo
ARCHIVE  = xstrp4.cma
NAME     = xstrp4
REQUIRES = bytes

all: $(ARCHIVE)

$(ARCHIVE): $(OBJECTS)
	$(OCAMLC) -a -o $(ARCHIVE) $(OBJECTS)


sample: sample.ml 
	ocamlfind ocamlc \
	  -package xstrp4 -syntax camlp4o -linkpkg \
	  sample.ml \
	  -o sample

view.sample: all
	camlp4 $(ROPTIONS) pa_o.cmo ./xstrp4.cma pr_o.cmo sample.ml

#----------------------------------------------------------------------
# general rules:

OPTIONS   =
OCAMLC    = $(OCAMLFIND) ocamlc -g $(STRING_OPTS) $(OPTIONS) $(CAMLP4_OPTS) -package "$(REQUIRES)"
OCAMLOPT  = $(OCAMLFIND) ocamlopt -g $(STRING_OPTS) $(OPTIONS) $(CAMLP4_OPTS) -package "$(REQUIRES)"
OCAMLDEP  = ocamldep $(OPTIONS)
OCAMLFIND = ocamlfind

#depend: *.ml *.mli
#	$(OCAMLDEP) *.ml *.mli >depend

*.mli:

.PHONY: install
install: all
	$(OCAMLFIND) install $(NAME) *.cmi *.cma META \
		-patch-version `./configure -version`

.PHONY: uninstall
uninstall:
	$(OCAMLFIND) remove $(NAME)

.PHONY: clean
clean:
	rm -f *.cmi *.cmo *.cma *.cmx *.o *.a *.cmxa sample

.PHONY: distclean
distclean: clean
	rm -f *~ depend depend.pkg

RELEASE: META
	awk '/version/ { print substr($$3,2,length($$3)-2) }' META >RELEASE

.SUFFIXES: .cmo .cmi .cmx .ml .mli .mll

.ml.cmx:
	$(OCAMLOPT) -c $<

.ml.cmo:
	$(OCAMLC) -c $<

.mli.cmi:
	$(OCAMLC) -c $<

.mll.ml:
	ocamllex $<

xstrp4_here_lexer.cmo: xstrp4_here_types.cmo xstrp4_here_types.cmi
xstrp4_here_lexer.cmi: xstrp4_here_types.cmi
xstrp4_here.cmo: xstrp4_here_lexer.cmo xstrp4_here_lexer.cmi
xstrp4_here.cmi: xstrp4_here_lexer.cmi
