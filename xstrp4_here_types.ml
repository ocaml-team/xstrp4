(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Camlp4
open PreCast
open Ast

type here_clause =
    Literal of (string * (Lexing.position * Lexing.position))
  | Variable of (ident * string * (Lexing.position * Lexing.position))
      (* [ M1; M2; ...; value ],f,pos1,pos2 
       * <==> M1.M2. ... .value with format f from position pos1 to pos2
       *)
  | Textend
;;



